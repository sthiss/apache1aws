output "vpc" {
  value = aws_vpc.vpc.id
}

output "cidrVpc" {
  value = aws_vpc.vpc.cidr_block
}

output "publicSubnets" {
  value = [aws_subnet.subnetPublic.id]
}

output "privateSubnets" {
  value = [aws_subnet.subnetPrivate[0], aws_subnet.subnetPrivate[1]]
}

output "publicRouteTableIds" {
  value = [aws_route_table.rtbPublic.id]
}

output "privateSubnetsIdA" {
  value = aws_subnet.subnetPrivate[0].id
}

output "privateSubnetsIdB" {
  value = aws_subnet.subnetPrivate[1].id
}

output "BastionPublicIp" {
  value = aws_instance.BastionInstance.public_ip
}

output "VaultPrivateIp" {
  value = aws_instance.vaultnstance.private_ip
}
