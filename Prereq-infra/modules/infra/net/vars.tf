variable cidrVpc {}
variable cidrPublicSubnet {}
variable cidrPrivateSubnet {}
variable Sg22Pub {}
variable environmentTag {}

variable key_name {}
variable vault_key_name {}

variable instanceType {
  description = "type for aws EC2 instance"
  default = "t2.micro"
}

variable azList {}


variable vaultInstanceProfileId {}
variable Sg22Priv {}
variable efsID {}
variable accesspointID {}

variable privateSubnetsIdA {}

variable Role {}
variable Project {}
variable Owner {}
variable region {}
