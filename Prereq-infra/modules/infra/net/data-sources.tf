data "aws_ami" "awsLinux" {
  most_recent = true
  filter {
    name = "name"
    values = ["amzn2-ami-hvm*"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["amazon"]
}

# Vault EC2 instance boot config template
data "template_file" "vaultInitTemplate" {
    template = file("${path.module}/userDataVault.tpl")
    vars = {
      EFS_ID = var.efsID
      ACCESSPOINT_ID = var.accesspointID
    }
}

data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true

# Execute the files we created
  part {
    filename     = "init.sh"
    content_type = "text/x-shellscript"
    content      = data.template_file.vaultInitTemplate.rendered
  }

}
