# Priv subnet route tables
resource "aws_route_table" "rtbPrivate" {
  vpc_id = aws_vpc.vpc.id
  route {
      cidr_block = "0.0.0.0/0"
      nat_gateway_id = aws_nat_gateway.ngwA.id
  }

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }

}

# Manually supplied AZ's
resource "aws_route_table_association" "rtaSubnetPrivate" {
  count = length(var.azList)
  subnet_id      = element(aws_subnet.subnetPrivate.*.id, count.index)
  route_table_id = aws_route_table.rtbPrivate.id
}

resource "aws_subnet" "subnetPrivate" {
  count = length(var.azList)
  vpc_id = aws_vpc.vpc.id
  cidr_block = element(var.cidrPrivateSubnet,count.index)
  map_public_ip_on_launch = "false"
  availability_zone = var.azList[count.index]
  tags = {
    Environment = var.environmentTag
    Name = "${var.environmentTag}-Private-Subnet-${count.index+1}"
  }
}
