resource "aws_instance" "vaultnstance" {
  ami = data.aws_ami.awsLinux.id
  instance_type = "t2.micro"
  iam_instance_profile = var.vaultInstanceProfileId

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
    Name = "${var.Project}-Vault"
  }

  user_data = data.template_cloudinit_config.config.rendered
  key_name = var.vault_key_name
  vpc_security_group_ids = [ var.Sg22Priv ]
  subnet_id = var.privateSubnetsIdA

  # prevent boottime config until routetables are ready
  depends_on = [
    aws_route_table_association.rtaSubnetPrivate
  ]

}
