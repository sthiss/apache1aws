# Network
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}

# One NAT used by priv subnet routetable - in reality we will have one EIP/NAT per AZ...
resource "aws_eip" "eipNatA" {
  vpc      = true

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}

resource "aws_nat_gateway" "ngwA" {
    allocation_id = aws_eip.eipNatA.id
    subnet_id     = aws_subnet.subnetPublic.id
    depends_on    = [aws_internet_gateway.igw]
}

resource "aws_route_table" "rtbPublic" {
  vpc_id = aws_vpc.vpc.id
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}

resource "aws_route_table_association" "rtaSubnetPublic" {
  subnet_id      = aws_subnet.subnetPublic.id
  route_table_id = aws_route_table.rtbPublic.id
}

resource "aws_subnet" "subnetPublic" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.cidrPublicSubnet
  map_public_ip_on_launch = "true"
  availability_zone = var.azList[0]

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}


