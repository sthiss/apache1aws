# Bastion for public subnet
resource "aws_instance" "BastionInstance" {
  ami = data.aws_ami.awsLinux.id
  instance_type = var.instanceType
  subnet_id = aws_subnet.subnetPublic.id
  vpc_security_group_ids = [var.Sg22Pub]
  key_name = var.key_name

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
    Name = "${var.Project}-Bastion"
  }
}
