# VPC
resource "aws_vpc" "vpc" {
  cidr_block = var.cidrVpc
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}
