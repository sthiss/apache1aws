#!/bin/bash -ex
# amzn-ami

# efs-utils
yum -y update
yum -y install amazon-efs-utils

## Mount efs to store the public sshkeys (used for authorizing ssh access to the Apache EC2 instance)
mkdir /mnt/efs
efs_id="${EFS_ID}"
accesspoint_id="${ACCESSPOINT_ID}"
mount -t efs -o tls,accesspoint=$accesspoint_id $efs_id:/ /mnt/efs
sudo chown ec2-user /mnt/efs && sudo chmod 775 /mnt/efs
