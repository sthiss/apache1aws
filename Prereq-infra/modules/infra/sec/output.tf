output "Sg22Pub" {
  value = aws_security_group.Sg22Pub.id
}

output "Sg22Priv" {
  value = aws_security_group.Sg22Priv.id
}

output "appSecgrpHttp" {
  value = aws_security_group.appSecgrpHttp.id
}

output "EfsSecgrp" {
  value = aws_security_group.EfsSecgrp.id
}
