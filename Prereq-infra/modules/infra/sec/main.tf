# SSH access to the bastion
resource "aws_security_group" "Sg22Pub" {
  name = "Sg22Pub-${var.environmentTag}"
  vpc_id = var.vpcId

  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.environmentTag
    name = "Sg22Pub"
  }
}

# SSH access from the VPC
resource "aws_security_group" "Sg22Priv" {
  name = "Sg22Priv-${var.environmentTag}"
  vpc_id = var.vpcId

  ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = [var.cidrVpc]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Environment = var.environmentTag
    name = "Sg22Priv"
  }
}

# Application secgrp http
resource "aws_security_group" "appSecgrpHttp" {
  name = "appSecgrpHttp-${var.environmentTag}"
  vpc_id = var.vpcId

  ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = [var.cidrVpc]
  }

  tags = {
    Environment = var.environmentTag
    name = "appSecgrpHttp"
  }
}

# EFS ingress from priv subnets
resource "aws_security_group" "EfsSecgrp" {
  name = "EfsSecgrp-${var.environmentTag}"
  vpc_id = var.vpcId

  ingress {
    security_groups = [aws_security_group.Sg22Priv.id]
    from_port   = 2049
    to_port     = 2049
    protocol    = "tcp"
  }

  egress {
    security_groups = [aws_security_group.Sg22Priv.id]
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.cidrVpc]
  }

  tags = {
    Environment = var.environmentTag
    name = "EfsSecgrp"
  }
}
