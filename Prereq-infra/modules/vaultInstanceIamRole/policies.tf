data "aws_iam_policy_document" "vault" {
  statement {
    actions = [
        "elasticfilesystem:*"
    ]
    resources = [
      var.efsARN
    ]
    effect = "Allow"
  }

  statement {
    actions = [
        "kms:ListAliases",
        "kms:GenerateDataKey",
        "kms:Decrypt",
        "kms:ListKeys",
        "kms:DescribeKey"
    ]
    resources = [
      "arn:aws:kms:::*"
    ]
    effect = "Allow"
  }

}
