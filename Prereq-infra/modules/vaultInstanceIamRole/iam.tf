resource "aws_iam_role" "vault-ec2" {
  name                  = "${var.Project}-VaultEc2InstanceProfile-${var.environmentTag}"
  force_detach_policies = true
  assume_role_policy    = data.aws_iam_policy_document.vault_assume_role.json

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}

resource "aws_iam_policy" "vault-ec2-policy" {
  name        = "${var.Project}-VaultEc2InstanceProfile-${var.environmentTag}"
  path        = "/"
  description = "Policy for vault instances"
  policy      = data.aws_iam_policy_document.vault.json

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}

resource "aws_iam_role_policy_attachment" "vault-ec2-policy-attachment" {
  role       = aws_iam_role.vault-ec2.name
  policy_arn = aws_iam_policy.vault-ec2-policy.arn
}

// Attach policies for the AmazonElasticFileSystemClientFullAccess
resource "aws_iam_role_policy_attachment" "vault-ec2-policy-attachment-efs" {
  role       = aws_iam_role.vault-ec2.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonElasticFileSystemClientFullAccess"
}

resource "aws_iam_instance_profile" "vaultInstanceProfile" {
  name = "vaultInstanceProfile-${var.environmentTag}"
  role = aws_iam_role.vault-ec2.name

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}
