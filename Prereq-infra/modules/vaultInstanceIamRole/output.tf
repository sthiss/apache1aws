output "vaultInstanceProfileArn" {
  value = aws_iam_instance_profile.vaultInstanceProfile.arn
}

output "vaultInstanceProfileId" {
  value = aws_iam_instance_profile.vaultInstanceProfile.id
}
