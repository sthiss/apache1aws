output "efsID" {
  value = aws_efs_file_system.foo.id
}

output "efsAccesspointID" {
  value = aws_efs_access_point.foo.id
}

output "efsMountPointID" {
  value = aws_efs_mount_target.alpha.id
}

output "efsARN" {
  value = aws_efs_file_system.foo.arn
}
