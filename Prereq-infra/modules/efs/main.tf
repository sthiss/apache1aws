resource "aws_efs_file_system" "foo" {
  creation_token = var.efsNamePrefix
  encrypted = true

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }

}

resource "aws_efs_access_point" "foo" {
  file_system_id = aws_efs_file_system.foo.id
}

resource "aws_efs_mount_target" "alpha" {
  file_system_id = aws_efs_file_system.foo.id
  subnet_id = var.privateSubnets[0].id
  security_groups = [var.EfsSecgrp]
}

resource "aws_efs_mount_target" "beta" {
  file_system_id = aws_efs_file_system.foo.id
  subnet_id = var.privateSubnets[1].id
  security_groups = [var.EfsSecgrp]
}

resource "aws_efs_file_system_policy" "policy" {
  file_system_id = aws_efs_file_system.foo.id

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "efs-policy-full",
    "Statement": [
        {
            "Sid": "efs-statement-principals",
            "Effect": "Allow",
            "Principal": {
                "AWS": "*"
            },
            "Resource": "${aws_efs_file_system.foo.arn}",
            "Action": [
                "elasticfilesystem:ClientRootAccess",
                "elasticfilesystem:ClientMount",
                "elasticfilesystem:ClientWrite"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "true"
                }
            }
        }
    ]
}
POLICY
}
