resource "aws_iam_role" "developers-role" {
  name                  = var.developersRoleName
  force_detach_policies = true
  assume_role_policy    = data.aws_iam_policy_document.developersTrustPolicy.json
  max_session_duration = 43200

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}

# Attach AWS managed policy AdministratorAccess - fine tune & lock it down with an SCP
resource "aws_iam_role_policy_attachment" "developersAccess" {
  role       = aws_iam_role.developers-role.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
