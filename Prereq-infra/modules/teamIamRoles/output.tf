output "developersIamRoleArn" {
  value = aws_iam_role.developers-role.arn
}

output "testersIamRoleArn" {
  value = aws_iam_role.testers-role.arn
}
