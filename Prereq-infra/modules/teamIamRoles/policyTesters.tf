data "aws_iam_policy_document" "testersTrustPolicy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = [var.awsAccount]
    }
  }
}
