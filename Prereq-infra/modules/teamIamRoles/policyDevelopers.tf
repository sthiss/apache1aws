data "aws_iam_policy_document" "developersTrustPolicy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = [var.awsAccount]
    }
  }
}
