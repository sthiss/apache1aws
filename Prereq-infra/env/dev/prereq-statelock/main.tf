terraform {
  required_version = ">= 1.0.1"
}

terraform {
  required_providers {
    aws = {
      version = "~> 3.53"
    }
  }
}

provider "aws" {
  region     = var.AWS_REGION
  profile    = var.Role
}

resource "aws_dynamodb_table" "terraformStateLock" {
  name           = "${var.Project}-${var.environmentTag}"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}
