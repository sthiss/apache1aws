variable "Role" {
  description = "AWS profile"
}

variable "AWS_REGION" {
  description = "AWS region"
}

variable "Project" {
  description = "Project Name"
}

variable "environmentTag" {
  description = "Environment"
}
