terraform {
  required_version = ">= 1.0.1"
}

terraform {
  required_providers {
    aws = {
      version = "~> 3.53"
    }
  }
}

provider "aws" {
  profile = var.Role
  region = var.AWS_REGION
}

# store tfstate in existing s3
terraform {
  backend "s3" {
    encrypt = false
  }
}

module "network" {
  source = "../../modules/infra/net"
  cidrVpc = var.cidrVpc
  cidrPublicSubnet = var.cidrPublicSubnet
  cidrPrivateSubnet = var.cidrPrivateSubnet
  Sg22Pub = module.security.Sg22Pub
  environmentTag = var.environmentTag
  azList = var.azList
  key_name = var.key_name
  vault_key_name = var.vault_key_name
  vaultInstanceProfileId = module.iam.vaultInstanceProfileId
  Sg22Priv = module.security.Sg22Priv
  efsID = module.efs.efsID
  accesspointID = module.efs.efsAccesspointID
  privateSubnetsIdA = module.network.privateSubnetsIdA
  region = var.AWS_REGION
  Role = var.Role
  Project = var.Project
  Owner = var.Owner
}

module "security" {
  source = "../../modules/infra/sec"
  cidrVpc = var.cidrVpc
  vpcId = module.network.vpc
  environmentTag = var.environmentTag
  region = var.AWS_REGION
  Role = var.Role
  Project = var.Project
  Owner = var.Owner
}

module "teamRoles" {
  source = "../../modules/teamIamRoles"
  environmentTag = var.environmentTag
  Project = var.Project
  awsAccount = var.awsAccount
  developersRoleName = var.developersRoleName
  testersRoleName = var.testersRoleName
  region = var.AWS_REGION
  Role = var.Role
  Owner = var.Owner
}

module "efs" {
  source = "../../modules/efs"
  efsNamePrefix = var.efsNamePrefix
  privateSubnets = module.network.privateSubnets
  EfsSecgrp = module.security.EfsSecgrp
  region = var.AWS_REGION
  Role = var.Role
  Project = var.Project
  Owner = var.Owner
}

module "iam" {
  source = "../../modules/vaultInstanceIamRole"
  environmentTag = var.environmentTag
  efsARN = module.efs.efsARN
  region = var.AWS_REGION
  Role = var.Role
  Project = var.Project
  Owner = var.Owner
}

