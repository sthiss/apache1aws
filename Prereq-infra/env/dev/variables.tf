variable "Role" {
  description = "AWS profile"
}

variable "AWS_REGION" {
  description = "AWS region"
}

variable "Project" {
  description = "Project Name"
}

variable "Owner" {
  description = "Owner email"
}

variable "environmentTag" {
  description = "Environment tag"
}

variable "cidrVpc" {
  description = "CIDR block for the VPC"
}

variable "cidrPublicSubnet" {
  description = "CIDR block for the public subnet"
}

variable "cidrPrivateSubnet" {
  type = list(string)
  description = "CIDR blocks for the private subnets"
}

variable "azList" {
  type = list(string)
  description = "list of AZ's to use in this region"
}

variable key_name {
  description = "ec2-sshkey to use for the bastion host"
}

variable vault_key_name {
  description = "ec2-sshkey to use for the vault host"
}

variable awsAccount {
  description = "Target AWS Account ID for these resources"
}

variable developersRoleName {
  description = "developersRoleName"
  default = "dev"
}

variable testersRoleName {
  description = "testersRoleName"
  default = "test"
}

variable "efsNamePrefix" {
  description = "EFS Name prefix - used to store public ssh-keys"
  default = "tmpTeamPubSshKeys"
}
