#10.208.0.1 - 10.208.255.254
cidrVpc = "10.208.0.0/16"

#10.208.0.1 - 10.208.0.254
cidrPublicSubnet = "10.208.0.0/24"

#10.208.1.1 - 10.208.3.254
cidrPrivateSubnet = ["10.208.1.0/24", "10.208.2.0/24"]

# Availability Zones list
azList = ["eu-west-1a", "eu-west-1b"]
