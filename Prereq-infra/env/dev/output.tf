output "environmentTag" {
  value = var.environmentTag
}

output "aws_region" {
  value = var.AWS_REGION
}

output "cidrVpc" {
  value = var.cidrVpc
}

output "cidrPublicSubnet" {
  value = var.cidrPublicSubnet
}

output "cidrPrivateSubnet" {
  value = var.cidrPrivateSubnet
}

output "privateSubnetsIdList" {
  value = module.network.privateSubnets.*.id
}

output "privateSubnetsIdA" {
  value = module.network.privateSubnetsIdA
}

output "privateSubnetsIdB" {
  value = module.network.privateSubnetsIdB
}

output "developersIamRoleArn" {
  value = module.teamRoles.developersIamRoleArn
}

output "testersIamRoleArn" {
  value = module.teamRoles.testersIamRoleArn
}

output "efsID" {
  value = module.efs.efsID
}

output "efsAccesspointID" {
  value = module.efs.efsAccesspointID
}

output "efsARN" {
  value = module.efs.efsARN
}

output "vaultInstanceProfileArn" {
  value = module.iam.vaultInstanceProfileArn
}

output "Sg22Priv" {
  value = module.security.Sg22Priv
}

output "appSecgrpHttp" {
  value = module.security.appSecgrpHttp
}

output "vpcId" {
  value = module.network.vpc
}

output "BastionPublicIp" {
  value = module.network.BastionPublicIp
}

output "VaultPrivateIp" {
  value = module.network.VaultPrivateIp
}
