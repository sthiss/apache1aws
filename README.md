# Apache HTTP server on AWS

## What is this repo
A repo containing Terraform to launch a private Apache HTTP webserver on AWS

## Requirements
- Terraform Version => 1.0.1
- aws-cli  Version => 2.2.14

## Repo structure details and navigation
This repo consists of 2 main terraform deployments: 
- The first deployment contains Terraform in the directory "Prereq-infra" - containing VPC/Subnet/Bastion/Vault-Instance/Iam-Roles/EFS-volume etc
- The second deployment contains Terraform in the directory "Apache"  - containing the Apache EC2 instances and dependencies 

## Assumptions
- For Step1/2 you will create the VPC/subnets/secgrps/bastion/vault-instance/IAMRoles
- For Step3 you will manually generate some ssh keys for users dev/test and upload them to an encrypted efs volume
- For Step4/5 you will deploy the main Apache Webserver components
- You have at your disposal 3 AWS EC2 keypairs for use with the Bastion/Vault-Instance/Apache EC2 instances (they can share for the sake of testing in a non-prod account)
- Anywhere you see >> -var="Role=admin" << it is assumed you replace this with a role having appropriate permissions to create AWS resources.
- Any commands listed in this README are for linux terminal (but should work closely if not the same for macOS)
- Your local ssh client has "ForwardAgent yes" option enabled - optionally you can pass the "-A" option on the cli
- If you choose NOT to use the resources provided by the Prereq-infra deployment (Step1/2), you may need modification to deploy Apache successfully (e.g. when managing pub sshkey locations)


## Before executing any Terraform commands
- Setup your VPC/subnet CIDR range, and AZ zones IF the following defaults do not work for you. Skip to the next section otherwise.

```
#10.208.0.1 - 10.208.255.254
cidrVpc = "10.208.0.0/16"

#10.208.0.1 - 10.208.0.254
cidrPublicSubnet = "10.208.0.0/24"

#10.208.1.1 - 10.208.3.254
cidrPrivateSubnet = ["10.208.1.0/24", "10.208.2.0/24"]

# Availability Zones list
azList = ["eu-west-1a", "eu-west-1b"]
```

- Please modify cidrVpc, cidrPublicSubnet, cidrPrivateSubnet in Prereq-infra/env/terraform.tfvars if you do not have 10.208 /24 space available
- Please modify azList in Prereq-infra/env/terraform.tfvars if eu-west-1 region does not work for you

## Step 1 "Prereq-infra" PREREQUISITE - generate a Terraform statelock file for use with the infrastructure deployment

```
cd Prereq-infra/env/dev/prereq-statelock

terraform init

terraform plan -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw-infra" \
  -var="environmentTag=dev" \
  -var="Role=admin"

terraform apply -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw-infra" \
  -var="environmentTag=dev" \
  -var="Role=admin"
```

- Note the output for the value "statelockTableId" you need it for the next step as an input param for "dynamodb_table"

## Step 2 "Prereq-infra" - Deploy the infrastructure resources
- Substitute your "dynamodb_table" (output from Step 1), "bucket" & "key_name"/"vault-key_name", "awsAccount", Owner values as needed

```
cd Prereq-infra/env/dev

terraform init -backend-config="region=eu-west-1" \
  -backend-config="bucket=<your-existing-statefile-bucket-name>" \
  -backend-config="key=apacheftw/apacheftw.state" \
  -backend-config="dynamodb_table=<output-value-statelockTableId-from-previous-step>" \
  -var="AWS_REGION=eu-west-1" \
  -var="Role=admin"

terraform plan -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw-infra" \
  -var="environmentTag=dev" \
  -var="Role=admin" \
  -var="key_name=<Your-bastion-ec2-ssh-keypair-name>"  \
  -var="vault_key_name=<Your-vault-ec2-ssh-keypair-name>" \
  -var="awsAccount=<1234567890>" \
  -var="Owner=ex@ex.com"

terraform apply -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw-infra" \
  -var="environmentTag=dev" \
  -var="Role=admin" \
  -var="key_name=<Your-bastion-ec2-ssh-keypair-name>" \
  -var="vault_key_name=<Your-vault-ec2-ssh-keypair-name>" \
  -var="awsAccount=<1234567890>" \
  -var="Owner=ex@ex.com"
```

- Take note of the outputs, you will need the values for Step 5

## Step 3 Setup user sshkey pairs - for authentication to Apache EC2 instance
- The aim here is to store users (dev/test) pub keys on an encrypted efs volume so Apache instance can add them at boot time to ~/$USER/.ssh/authorized_keys
- Generate keys for 2 users 'test' and 'dev'
- Do not supply a password to allow password-less logins

```
cd ~/.ssh/

ssh-keygen -m PEM -f apacheftw-dev.pem
ssh-keygen -m PEM -f apacheftw-test.pem
```

- Add required user ssh-key to your agent (dev or test as above), connect to you bastion host, and subsequently the Vault instance hosting the efs volume:
```
ssh-add -k apacheftw-dev.pem

ssh -i "<Your-bastion-ec2-ssh-keypair-name>" <BastionPublicIp-Output-From-Step2>

ssh ec2-user@<VaultPrivateIp-Output-From-Step2>
```

- Change directory to the efs volume, and store the pub keys in their user owned directories
```
mkdir /mnt/efs/dev
vi /mnt/efs/dev/apacheftw-dev.pem.pub
<paste the contents of apacheftw-dev.pem.pub generated above>

mkdir /mnt/efs/test
vi /mnt/efs/test/apacheftw-test.pem.pub
<paste the contents of apacheftw-test.pem.pub generated above>
```

## Step 4 "Apache" PREREQUISITE - generate a Terraform statelock file for use with the Apache deployment

```
cd Apache/env/dev/prereq-statelock

terraform init

terraform plan -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw" \
  -var="environmentTag=dev" \
  -var="Role=admin"

terraform apply -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw" \
  -var="environmentTag=dev" \
  -var="Role=admin"
```

## Step 5 "Apache" - Deploy the Apache resources
- Choose dev or test IAM role to deploy Apache - (these were generated in Step 2), then update the >> -var="Role=dev" << input param accordingly below
- Configure your ~/.aws config/credentials && authenticate to AWS with this Role
- Replace "Output-From-Step2" with the appropriate Terraform outputs shown from Step 2
- Replace "your-apache-ec2-ssh-keypair-name" with your own (a valid sshkey name used for the Apache instance)
- Replace "your-existing-statefile-bucket-name" with your own

```
cd Apache/env/dev/prereq-statelock

terraform init -backend-config="region=eu-west-1" \
  -backend-config="bucket=<your-existing-statefile-bucket-name>" \
  -backend-config="key=apacheftw/apacheftw.state" \
  -backend-config="dynamodb_table=apacheftw-dev" \
  -var="AWS_REGION=eu-west-1" \
  -var="Role=dev"

terraform plan -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw" \
  -var="environmentTag=dev" \
  -var="Role=dev" \
  -var="devRoleArn=arn:aws:iam::1234567890:role/dev-Output-From-Step2" \
  -var="testRoleArn=arn:aws:iam::1234567890:role/test-Output-From-Step2" \
  -var="key_name=<your-apache-ec2-ssh-keypair-name>" \
  -var="efsID=Output-From-Step2" \
  -var="accesspointID=Output-From-Step2" \
  -var="efsARN=Output-From-Step2" \
  -var='privateSubnetsIdList=["subnet-Output-From-Step2", "subnet-Output-From-Step2"]' \
  -var="vpcId=Output-From-Step2" \
  -var="Sg22Priv=Output-From-Step2" \
  -var="appSecgrpHttp=Output-From-Step2" \
  -var="Owner=ex@ex.com"

# Test deploy with dev Role
terraform apply -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw" \
  -var="environmentTag=dev" \
  -var="Role=dev" \
  -var="devRoleArn=arn:aws:iam::1234567890:role/dev-Output-From-Step2" \
  -var="testRoleArn=arn:aws:iam::1234567890:role/test-Output-From-Step2" \
  -var="key_name=<your-apache-ec2-ssh-keypair-name>" \
  -var="efsID=Output-From-Step2" \
  -var="accesspointID=Output-From-Step2" \
  -var="efsARN=Output-From-Step2" \
  -var='privateSubnetsIdList=["subnet-Output-From-Step2", "subnet-Output-From-Step2"]' \
  -var="vpcId=Output-From-Step2" \
  -var="Sg22Priv=Output-From-Step2" \
  -var="appSecgrpHttp=Output-From-Step2" \
  -var="Owner=ex@ex.com"
```

## Get the Apache Instance private IP's 
- Replace the value "AutoScalingGroupName" below for AutoScalingGroupName output from Step 5

```
aws autoscaling describe-auto-scaling-instances --region eu-west-1 --output text \
--query "AutoScalingInstances[?AutoScalingGroupName=='AutoScalingGroupName-Output-From-Step5'].InstanceId" \
| xargs -n1 aws ec2 describe-instances --instance-ids $ID --region eu-west-1 \
--query "Reservations[].Instances[].PrivateIpAddress" --output text
```

## Step 6 Cleanup - Destroy all AWS resources

```
cd Apache/env/dev/

terraform apply -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw" \
  -var="environmentTag=dev" \
  -var="Role=admin" \
  -var="devRoleArn=arn:aws:iam::1234567890:role/dev-Output-From-Step2" \
  -var="testRoleArn=arn:aws:iam::1234567890:role/test-Output-From-Step2" \
  -var="key_name=<your-apache-ec2-ssh-keypair-name>" \
  -var="efsID=Output-From-Step2" \
  -var="accesspointID=Output-From-Step2" \
  -var="efsARN=Output-From-Step2" \
  -var='privateSubnetsIdList=["subnet-Output-From-Step2", "subnet-Output-From-Step2"]' \
  -var="vpcId=Output-From-Step2" \
  -var="Sg22Priv=Output-From-Step2" \
  -var="appSecgrpHttp=Output-From-Step2" \
  -var="Owner=ex@ex.com"


cd Apache/env/dev/prereq-statelock

terraform destroy -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw" \
  -var="environmentTag=dev" \
  -var="Role=admin"


cd Prereq-infra/env/dev/

terraform destroy -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw-infra" \
  -var="environmentTag=dev" \
  -var="Role=admin" \
  -var="key_name=<Your-bastion-ec2-ssh-keypair-name>" \
  -var="vault_key_name=<Your-vault-ec2-ssh-keypair-name>" \
  -var="awsAccount=<1234567890>" \
  -var="Owner=ex@ex.com"


cd Prereq-infra/env/dev/prereq-statelock

terraform destroy -var="AWS_REGION=eu-west-1" \
  -var="Project=apacheftw" \
  -var="environmentTag=dev" \
  -var="Role=admin"
```
