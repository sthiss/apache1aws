variable environmentTag {}
variable privateSubnetsIdList {}
variable Sg22Priv {}
variable appSecgrpHttp {}
variable apacheInstanceProfile {}
variable vpcId {}

variable "appInstType" {
  description = "appInstType"
  default = "t2.micro"
}

variable key_name {
  description = "ec2-key to use"
}

variable asg_min_size {}
variable asg_max_size {}
variable efsID {}

variable accesspointID {}
variable Role {}
variable Project {}
variable Owner {}
variable region {}
