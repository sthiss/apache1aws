resource "aws_route53_zone" "private" {
  name = "${var.environmentTag}-services.ac"

  vpc {
    vpc_id = var.vpcId
  }
}

resource "aws_route53_record" "app" {
  zone_id = aws_route53_zone.private.zone_id
  name    = "app.${var.environmentTag}-services.ac"
  type    = "CNAME"
  ttl     = "300"
  records = [aws_lb.appNlb.dns_name]
}
