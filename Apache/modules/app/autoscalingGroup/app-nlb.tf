resource "aws_lb" "appNlb" {
  name               = "${var.environmentTag}-nlb"
  internal           = true
  load_balancer_type = "network"
  subnets            = var.privateSubnetsIdList.*

  enable_deletion_protection = false

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}

resource "aws_lb_target_group" "appNlbTg" {
  name     = "${var.environmentTag}-nlbTg"
  port     = 80
  protocol = "TCP"
  vpc_id   = var.vpcId

  health_check {
    protocol = "TCP"
    port = 80
  }
}

resource "aws_lb_listener" "appNlbListener" {
  load_balancer_arn = aws_lb.appNlb.arn
  port              = 80
  protocol          = "TCP"


  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.appNlbTg.arn
  }
}
