# Get latest Amazon AMI:
data "aws_ami" "awsLinux" {
  most_recent = true
  filter {
    name = "name"
    values = ["amzn2-ami-hvm*"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["amazon"]
}

# combine all template_file into template_cloudinit_config
data "template_file" "appLcTemplate" {
    template = file("${path.module}/userDataApp.tpl")
    vars = {
      TMP_HOST_ID = "tmp_client1"
      EFS_ID = var.efsID
      ACCESSPOINT_ID = var.accesspointID
    }
}

data "template_file" "appLcWritefiles" {
    template = file("${path.module}/userDataWritefiles.tpl")
}

data "template_file" "appLcUptime" {
    template = file("${path.module}/userDataUptime.tpl")
}

data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true

# Write some files we intend to use
  part {
    filename     = "writefiles.sh"
    content_type = "text/cloud-config"
    content      = data.template_file.appLcWritefiles.rendered
  }

# Execute the files we created
  part {
    filename     = "init.sh"
    content_type = "text/x-shellscript"
    content      = data.template_file.appLcTemplate.rendered
  }

# Did it take long?
  part {
    filename     = "boottime.sh"
    content_type = "text/cloud-config"
    content      = data.template_file.appLcUptime.rendered
  }

}
