#!/bin/bash -ex
# amzn-ami

PRIVATE_IP=$(curl http://169.254.169.254/latest/meta-data/local-ipv4/)
EC2_INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id/)
INTERFACE=$(curl --silent http://169.254.169.254/latest/meta-data/network/interfaces/macs/)
VPC_ID=$(curl --silent http://169.254.169.254/latest/meta-data/network/interfaces/macs/$INTERFACE/vpc-id)
NODE_ID=$(hostid)

## Httpd, efs-utils
yum -y update
yum -y install httpd amazon-efs-utils
echo "Can you pronounce the coolest city in Wales: Llanfairpwllgwyngyllgogerychwyrndrobwllllantysiliogogogoch?" >> /var/www/html/index.html
service httpd start

# local vars from MetaData
CLIENT_NAME+="Apache_$VPC_ID"

sed -i "/name = / s#=.*#=$CLIENT_NAME#" /etc/clientname.txt

## Mount efs to get teh pubkeys
mkdir /mnt/efs
efs_id="${EFS_ID}"
accesspoint_id="${ACCESSPOINT_ID}"
mount -t efs -o tls,accesspoint=$accesspoint_id $efs_id:/ /mnt/efs

# depending on the user - create a homedir && copy over the appropriate pub sshkey from efs
useradd -d /home/dev "dev"
mkdir /home/dev/.ssh && touch /home/dev/.ssh/authorized_keys
cat /mnt/efs/dev/apacheftw-dev.pem.pub >> /home/dev/.ssh/authorized_keys
sudo chmod 644 /home/dev/.ssh/authorized_keys
sudo chown -R "dev":"dev" /home/dev/

useradd -d /home/test "test"
mkdir /home/test/.ssh && touch /home/test/.ssh/authorized_keys
cat /mnt/efs/test/apacheftw-test.pem.pub >> /home/test/.ssh/authorized_keys
sudo chmod 644 /home/test/.ssh/authorized_keys
sudo chown -R "test":"test" /home/test/
