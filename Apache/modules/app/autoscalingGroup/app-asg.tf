locals {
  standard_tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
    InstanceProfileRole = "${var.Project}-app-ec2"
  }
}

resource "aws_autoscaling_group" "appAsg" {
    name = "${var.environmentTag}-${aws_launch_configuration.webLc.id}"
    launch_configuration = aws_launch_configuration.webLc.id
    min_size = var.asg_min_size
    max_size = var.asg_max_size
    desired_capacity = var.asg_max_size
    default_cooldown = 60

    vpc_zone_identifier = var.privateSubnetsIdList.*

    lifecycle {
        create_before_destroy = true
    }

    tag {
        key                 = "Name"
        value               = "${var.Project}-${var.environmentTag}"
        propagate_at_launch = true
    }

    dynamic "tag" {
        for_each = local.standard_tags

        content {
        key                 = tag.key
        value               = tag.value
        propagate_at_launch = true
        }
    }

    target_group_arns = [aws_lb_target_group.appNlbTg.arn]
    health_check_type = "ELB"

}
