resource "aws_launch_configuration" "webLc" {
    name_prefix = "${var.environmentTag}-lc-"
    image_id = data.aws_ami.awsLinux.id
    instance_type = var.appInstType
    security_groups  = [
            var.Sg22Priv,
            var.appSecgrpHttp
    ]

    iam_instance_profile = var.apacheInstanceProfile

    key_name = var.key_name

    user_data = data.template_cloudinit_config.config.rendered
    enable_monitoring = false

    lifecycle {
        create_before_destroy = true
    }

}
