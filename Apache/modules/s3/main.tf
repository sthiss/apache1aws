resource "aws_s3_bucket" "s3" {
  bucket = "${var.Project}-${var.environmentTag}"
  acl    = "private"

  versioning {
    enabled = true
  }

  # Uncomment this for Prod
  # lifecycle {
  #   prevent_destroy = true
  # }

  # Comment this for Prod
  force_destroy = true

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }

}
