resource "aws_s3_bucket_policy" "this" {
	bucket = aws_s3_bucket.s3.id

	policy = <<POLICY
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Action": [
				"s3:*"
			],
			"Effect": "Allow",
			"Principal": {
				"AWS": [
					"${var.devRoleArn}"
				]
			},
			"Resource": [
				"${aws_s3_bucket.s3.arn}",
				"${aws_s3_bucket.s3.arn}/*"
			]
		},
		{
			"Action": [
				"s3:Get*"
			],
			"Effect": "Allow",
			"Principal": {
				"AWS": [
					"${var.testRoleArn}"
				]
			},
			"Resource": [
				"${aws_s3_bucket.s3.arn}",
				"${aws_s3_bucket.s3.arn}/*"
			]
		}
	]
}
POLICY
}
