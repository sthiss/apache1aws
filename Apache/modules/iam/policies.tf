data "aws_iam_policy_document" "apachetest" {
  statement {
    actions = [
      "elasticfilesystem:*",
    ]
    resources = [
      var.efsARN
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "kms:ListAliases",
      "kms:GenerateDataKey",
      "kms:Decrypt",
      "kms:ListKeys",
      "kms:DescribeKey"
    ]
    resources = [
      "arn:aws:kms:::*"
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "s3:GetBucketPolicy",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:ListBucketMultipartUploads"
    ]
    resources = [
      "${var.s3BucketArn}/*",
      var.s3BucketArn
    ]
    effect = "Allow"
  }

}

data "aws_iam_policy_document" "apachedev" {
  statement {
    actions = [
      "elasticfilesystem:*",
    ]
    resources = [
      var.efsARN
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "kms:ListAliases",
      "kms:GenerateDataKey",
      "kms:Decrypt",
      "kms:ListKeys",
      "kms:DescribeKey"
    ]
    resources = [
      "arn:aws:kms:::*"
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "s3:PutObject",
      "s3:GetBucketPolicy",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:ListBucketMultipartUploads"
    ]
    resources = [
      "${var.s3BucketArn}/*",
      var.s3BucketArn
    ]
    effect = "Allow"
  }

  statement {
    actions = [
      "ec2:*"
    ]
    resources = [
      "*"
    ]
    effect = "Allow"
  }

}
