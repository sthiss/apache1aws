resource "aws_iam_role" "test" {
  name                  = "${var.Project}-test-${var.environmentTag}"
  force_detach_policies = true
  assume_role_policy    = data.aws_iam_policy_document.app_assume_role.json

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}

resource "aws_iam_policy" "test" {
  name        = "${var.Project}-test-${var.environmentTag}"
  path        = "/"
  description = "Policy for Apache instances"
  policy      = data.aws_iam_policy_document.apachetest.json
}

resource "aws_iam_role_policy_attachment" "test-policy-attachment" {
  role       = aws_iam_role.test.name
  policy_arn = aws_iam_policy.test.arn
}

resource "aws_iam_role" "dev" {
  name                  = "${var.Project}-dev-${var.environmentTag}"
  force_detach_policies = true
  assume_role_policy    = data.aws_iam_policy_document.app_assume_role.json

  tags = {
    Role = var.Role
    Owner = var.Owner
    Project = var.Project
    region = var.region
  }
}

resource "aws_iam_policy" "dev" {
  name        = "${var.Project}-dev-${var.environmentTag}"
  path        = "/"
  description = "Policy for Apache instances"
  policy      = data.aws_iam_policy_document.apachedev.json
}

resource "aws_iam_role_policy_attachment" "dev-policy-attachment" {
  role       = aws_iam_role.dev.name
  policy_arn = aws_iam_policy.dev.arn
}

# Attach the appropriate instance profile based on the Role
resource "aws_iam_instance_profile" "apacheInstanceProfile" {
  name = "apacheInstanceProfile-${var.Project}-${var.Role}"
  role = "${var.Project}-${var.Role}-${var.environmentTag}"
}
