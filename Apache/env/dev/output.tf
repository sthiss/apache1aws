output "environmentTag" {
  value = var.environmentTag
}

output "aws_region" {
  value = var.AWS_REGION
}

output "ApacheAutocsalingGroupName" {
  value = module.app1Asg.ApacheAutocsalingGroupName
}
