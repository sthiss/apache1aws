terraform {
  required_version = ">= 1.0.1"
}

terraform {
  required_providers {
    aws = {
      version = "~> 3.53"
    }
  }
}

provider "aws" {
  profile = var.Role
  region = var.AWS_REGION
}

# store tfstate in existing s3
terraform {
  backend "s3" {
    encrypt = false
  }
}

module "s3" {
  source = "../../modules/s3"
  environmentTag = var.environmentTag
  devRoleArn = var.devRoleArn
  testRoleArn = var.testRoleArn
  region = var.AWS_REGION
  Role = var.Role
  Project = var.Project
  Owner = var.Owner
}

module "iam" {
  source = "../../modules/iam"
  environmentTag = var.environmentTag
  s3BucketArn = module.s3.s3BucketArn
  efsARN = var.efsARN
  region = var.AWS_REGION
  Role = var.Role
  Project = var.Project
  Owner = var.Owner
}

module "app1Asg" {
  source = "../../modules/app/autoscalingGroup"
  environmentTag = var.environmentTag
  privateSubnetsIdList = var.privateSubnetsIdList
  Sg22Priv = var.Sg22Priv
  appSecgrpHttp = var.appSecgrpHttp
  apacheInstanceProfile = module.iam.apacheInstanceProfile
  vpcId = var.vpcId
  asg_min_size = var.asg_min_size
  asg_max_size = var.asg_max_size
  efsID = var.efsID
  accesspointID = var.accesspointID
  key_name = var.key_name
  region = var.AWS_REGION
  Role = var.Role
  Project = var.Project
  Owner = var.Owner
}
