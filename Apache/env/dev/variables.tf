variable "Role" {
  description = "AWS profile"
  default = "dev"
}

variable "AWS_REGION" {
  description = "AWS region"
}

variable "Project" {
  description = "Project Name"
}

variable "Owner" {
  description = "Owner email"
}

variable "environmentTag" {
  description = "Environment tag"
}

variable "asg_min_size" {
  description = "ASG min"
}

variable "asg_max_size" {
  description = "ASG max"
}

variable key_name {
  description = "ec2-sshkey to use for the APACHE host"
}

variable "efsID" {
  description = "efsID holding pubkeys mounted on apache instance - fs-..."
}

variable "efsARN" {
  description = "efsARN"
}
variable "accesspointID" {
  description = "efs accesspoint ID - fsap-..."
}

variable "devRoleArn" {
  description = "devRoleArn for s3 bucket policy"
}

variable "testRoleArn" {
  description = "testRoleArn for s3 bucket policy"
}

variable "vpcId" {
  description = "vpcId"
}

variable "Sg22Priv" {
  description = "SSH access from within the VPC"
}

variable "appSecgrpHttp" {
  description = "HTTP access to APACHE EC2 instance in private subnet"
}

variable "privateSubnetsIdList" {
  type = list(string)
  description = "Private subnet list (2)"
}
